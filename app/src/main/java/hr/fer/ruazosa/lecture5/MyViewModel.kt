package hr.fer.ruazosa.lecture5

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyViewModel: ViewModel() {

    val resultOfDataFetch = MutableLiveData<String>()

    fun fetchDataFromRepository() {
            viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    for (i in 0..10) {
                        val fetchedResult = MyRepository.fetchData(i)
                        withContext(Dispatchers.Main) {
                            resultOfDataFetch.value = fetchedResult
                        }
                    }
                }
            }
    }

}