package hr.fer.ruazosa.lecture5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(MyViewModel::class.java)

        myViewModel.resultOfDataFetch.observe(this, Observer {
            operationStatusTextView.text = it
        })

        startLongRunningOperationButton.setOnClickListener {
            myViewModel.fetchDataFromRepository()
        }

        sayHelloButton.setOnClickListener {
            val toastMessage = Toast.makeText(applicationContext, "Hello world", Toast.LENGTH_LONG)
            toastMessage.show()

        }

    }
}
